FROM jgcl88/alpine-nginx-php

COPY ./app /app

RUN apk --update add gd php7-gd \
    && chmod +x /app/docker/start.sh \
    && composer install -vvv \
    && chown -R www:www /app

EXPOSE 80

CMD ["/app/docker/start.sh"]