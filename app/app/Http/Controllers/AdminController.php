<?php

namespace App\Http\Controllers;

use App\Imagens;
use App\Posts;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

//use Illuminate\Support\Facades\Mail;
//use App\Config;
//use Validator;
//use App\Http\Controllers\Route;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        return view('admin.dashboard');
    }

    //Postagens

    public function postsShow() {
        $posts = Posts::orderBy("ordem", "asc")->get();
        return view('admin.posts.listar', compact('posts'));
    }

    public function postsCreate() {
        return view('admin.posts.cadastrar');
    }

    public function postsEdit($id) {
        $post = Posts::find($id);
        return view('admin.posts.cadastrar', compact('post'));
    }

    //Imagens
    public function imagensShow() {
        $imagens = Imagens::orderBy("data", "desc")->get();
        return view('admin.imagens.listar', compact("imagens"));
    }

    
}
