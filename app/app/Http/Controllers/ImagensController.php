<?php

namespace App\Http\Controllers;

use App\Imagens;
use App\Posts;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Facades\Datatables;
use Cache;

class ImagensController extends Controller
{

	public function __construct()
	{
		
	}

	public function html_lista() {
		$imagens = Imagens::orderBy("data", "desc")->get();
		return view('admin.imagens.html', compact("imagens"));
	}

	public function DropzoneAction(Request $request) {
		$file = $request->file('file');

		$nome_original = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

		$nome = strtolower(md5($nome_original . time()) .".". $file->getClientOriginalExtension());
		$type = $file->getClientMimeType();

		$arquivoConteudo = file_get_contents($file);

		$arquivoPath = "imagens/" . $nome;

		$disco = env('FILESYSTEM_ORANGE');

		$envio = Storage::disk($disco)->put($arquivoPath, $arquivoConteudo, "public");

		$arquivoPath = "uploads/" . $arquivoPath;

		if($envio) {
			$titulo = $request->titulo;
			$data = $request->data;

			if(!$titulo) {
				$titulo = "Sem Título";
			}

			if(!$data) {
				$data = date("Y-m-d");
			} else {
				$data = str_replace('/', '-', $data);
			}

			$imagem = new Imagens;

			$imagem = $imagem->create([
				"url" => $arquivoPath,
				"titulo" => $titulo,
				"data" => date("Y-m-d", strtotime($data))
			]);

			Cache::flush();

			if($imagem->id) {
				return Response()->json([
					'id' => $imagem->id,
					'code' => 200,
					'message' => 'O cadastro foi realizado com sucesso.'
				], 200);
			} else {
				return Response()->json([
					'code' => 400,
					'message' => 'Houve um erro no cadastro.'
				], 400);
			}
		} else {
			return "false";
		}
	}

	public function update($id, Request $request)
	{
		$imagem = Imagens::findOrFail($id);
		$imagem->data = date("Y-m-d", strtotime(str_replace('/', '-', $request->data)));
		$imagem->titulo = $request->titulo;
		$sucesso = $imagem->update();

		if($sucesso) {
			return Response()->json([
				'id' => $imagem->id,
				'code' => 200,
				'message' => 'O cadastro foi editado com sucesso.'
			], 200);
		} else {
			return Response()->json([
				'code' => 400,
				'message' => 'Houve um erro na edição.'
			], 400);
		}
	}

	public function destroy($id)
	{
		$imagem = Imagens::findOrFail($id);

		$disco = env('FILESYSTEM_ORANGE');

		$url = explode("uploads/", $imagem->url);

		Storage::disk($disco)->delete($url[1]);

		$imagem->delete();

		Cache::flush();

		if($imagem->id) {
			return Response()->json([
				'id' => $imagem->id,
				'code' => 200,
				'message' => 'O registro foi deletado com sucesso.'
			], 200);
		} else {
			return Response()->json([
				'code' => 400,
				'message' => 'Houve um erro na exclusão.'
			], 400);
		}
	}


}