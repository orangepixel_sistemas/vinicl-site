<?php

namespace App\Http\Controllers;

use App\Posts;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Route;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Facades\Datatables;
use Cache;

class PostsController extends Controller
{

	public function __construct()
	{
		
	}

	public function datatables_ajax()
	{
		//return Datatables::of(Campeonatos::query())->make(true);
		return Datatables::queryBuilder(
					DB::table('posts')
					->select('*')
		)->make(true);
	}

	public function store(Request $request)
	{
		/*$input = $request->all();

		if ($request->imagem) {
			$imageName = $request->nome . "." . $request->imagem->getClientOriginalExtension();
			$request->file('imagem')->move(base_path() . '/public/site/img/post/', $imageName);

			$input['imagem'] = 'site/img/post/' . $imageName;
		}

		$id = Posts::create($input)->id;

		\Session::flash('message', 'Postagem criada com sucesso! ID:('.$id.')');
		return Redirect::route('posts.table');*/
		$input = $request->all();

		$validator = Validator::make($input, Posts::$rules);

		if ($validator->fails()) {
			$erros = $validator->errors()->all();
			
			return Response()->json([
				'code' => 400,
				'message' => $erros
			], 400);
		}

		$post = new Posts;

		//pego o usuario
		$user = Auth::user();
		$userid = $user->id;

		$input["id_usuario"] = $userid;

		$post = $post->create($input);

		Cache::flush();

		if($post->id) {
			return Response()->json([
				'id' => $post->id,
				'code' => 200,
				'message' => 'O cadastro foi realizado com sucesso.'
			], 200);
		} else {
			return Response()->json([
				'code' => 400,
				'message' => 'Houve um erro no cadastro.'
			], 400);
		}
	}

	public function update($id, Request $request)
	{
		$input = $request->all();

		$post = Posts::findOrFail($id);
		$post->update($input);

		Cache::flush();

		if($post->id) {
			return Response()->json([
				'id' => $post->id,
				'code' => 200,
				'message' => 'A edição foi realizada com sucesso.'
			], 200);
		} else {
			return Response()->json([
				'code' => 400,
				'message' => 'Houve um erro na edição.'
			], 400);
		}
	}

	public function updateOrdenacao(Request $request)
	{
		$ordenacao = $request->sortable;
		//$posts = Posts::get()->keyBy("id");

		$retorno = "";
		foreach($ordenacao as $posicao=>$id) {
			//$posts[$id]->ordem = $posicao+1;
			$retorno .= "ID: " .$id. " - Posic: " . ($posicao + 1) . "<br />";
			$post = Posts::findOrFail($id)->update(["ordem" => ($posicao + 1)]);
		}

		Cache::flush();

		return Response()->json([
			'code' => 200,
			'message' => 'A edição foi realizada com sucesso.'
		], 200);
	}

	public function destroy($id)
	{
		$post = Posts::findOrFail($id);

		$post->delete();

		Cache::flush();

		if($post->id) {
			return Response()->json([
				'id' => $post->id,
				'code' => 200,
				'message' => 'O registro foi deletado com sucesso.'
			], 200);
		} else {
			return Response()->json([
				'code' => 400,
				'message' => 'Houve um erro na exclusão.'
			], 400);
		}
	}
}
