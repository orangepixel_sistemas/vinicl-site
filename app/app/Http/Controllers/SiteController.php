<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Config;
use Validator;
use App\Http\Controllers\Route;
use App\Posts;
use App\Imagens;


class SiteController extends Controller
{

    public function __construct()
    {
        
    }

    public function index()
    {
        $posts = Posts::where("mostrar_pagina_inicial", "=", "1")->orderBy("ordem", "asc")->get();
        foreach($posts as $post) {
            $titulo = explode("/", $post->titulo);
            if(isset($titulo[1])) {        
                $post->titulo = $titulo[0] . "/<i>" . $titulo[1] . "</i>";
            }
        }

        $imagens = Imagens::orderBy("data", "desc")->get();
    	return view("site.index", compact("posts", "imagens"));
    }

    public function pesquisa()
    {
    	return view("site.pesquisa");
    }

    public function email(Request $request)
    {
        $data["subject"] = $request->subject;
        $data["name"] = $request->name;
        $data["conteudo"] = $request->message;
        $data["email"] = $request->email;

        $data["subject"] = $request->subject;
        $a = Mail::queue('email', ["data"=>$data], function ($message) {
            $message->subject("Contato no Site");
            //$message->sender($data["email"], $data["name"]);
            $message->from('laravel@orangepixel.com.br', 'OrangePixel');

            $to = env('FORM_EMAIL_TO', 'vinicius.laass@hotmail.com');
            $message->to($to);
        });
        return $a;
    }

    public function teste(Request $request)
    {
        $a = '{"0":{"href":"http://viniciuslaass.tk/img/galeria/24.jpg","title":"I Workshop de Álgebra e Topologia Algébrica da Bahia","data":"2016-02"},"1":{"href":"http://viniciuslaass.tk/img/galeria/1.jpg","title":"Defesa de Doutorado","data":"2015-07"},"2":{"href":"http://viniciuslaass.tk/img/galeria/2.jpg","title":"Defesa de Doutorado","data":"2015-07"},"3":{"href":"http://viniciuslaass.tk/img/galeria/3.jpg","title":"Defesa de Doutorado","data":"2015-07"},"4":{"href":"http://viniciuslaass.tk/img/galeria/4.jpg","title":"Winter Braids V - Pau, França","data":"2015-02"},"5":{"href":"http://viniciuslaass.tk/img/galeria/5.jpg","title":"I Encontro de Álgebra e Topologia Algébrica da UFSCAr, São Carlos SP","data":"2015-02"},"6":{"href":"http://viniciuslaass.tk/img/galeria/6.jpg","title":"Feriado de um matemático","data":"2014-08"},"7":{"href":"http://viniciuslaass.tk/img/galeria/8.jpg","title":"XIX Encontro Brasileiro de Topologia, São José do Rio Preto SP","data":"2014-08"},"8":{"href":"http://viniciuslaass.tk/img/galeria/7.jpg","title":"XIX Encontro Brasileiro de Topologia - Palestra","data":"2014-08"},"9":{"href":"http://viniciuslaass.tk/img/galeria/9.jpg","title":"Trem Bala Zurique - Paris","data":"2014-05"},"10":{"href":"http://viniciuslaass.tk/img/galeria/10.jpg","title":"Trem Bala Zurique - Paris","data":"2014-05"},"11":{"href":"http://viniciuslaass.tk/img/galeria/11.jpg","title":"Trem Bala Zurique - Paris","data":"2014-05"},"12":{"href":"http://viniciuslaass.tk/img/galeria/12.jpg","title":"Trem Bala Zurique - Paris","data":"2014-05"},"13":{"href":"http://viniciuslaass.tk/img/galeria/14.jpg","title":"Université de Caen, França","data":"2014-03"},"14":{"href":"http://viniciuslaass.tk/img/galeria/15.jpg","title":"Université de Caen, França - Tranway","data":"2014-03"},"15":{"href":"http://viniciuslaass.tk/img/galeria/13.jpg","title":"Université de Caen, França - Comida Boa =P","data":"2014-03"},"16":{"href":"http://viniciuslaass.tk/img/galeria/16.jpg","title":"XVIII Encontro Brasileiro de Topologia, Águas de Lindoia SP","data":"2012-08"},"17":{"href":"http://viniciuslaass.tk/img/galeria/17.jpg","title":"Defesa de Mestrado","data":"2011-02"},"18":{"href":"http://viniciuslaass.tk/img/galeria/18.jpg","title":"Defesa de Mestrado - Arguição da Banca","data":"2011-02"},"19":{"href":"http://viniciuslaass.tk/img/galeria/19.jpg","title":"Defesa de Mestrado - Turma boa","data":"2011-02"},"20":{"href":"http://viniciuslaass.tk/img/galeria/20.jpg","title":"Simpósio de Topologia Algébrica, Brotas SP","data":"2010-10"},"21":{"href":"http://viniciuslaass.tk/img/galeria/21.jpg","title":"XVII Encontro Brasileiro de Topologia, Rio de Janeiro RJ","data":"2010-08"},"22":{"href":"http://viniciuslaass.tk/img/galeria/22.jpg","title":"Colóquio Brasileiro de Matemática,  Rio de Janeiro RJ","data":"2009-07"},"23":{"href":"http://viniciuslaass.tk/img/galeria/23.jpg","title":"Colóquio Brasileiro de Matemática - Bons papos","data":"2009-07"}}';

    	$a = json_decode($a, true);

        $b = "INSERT INTO `imagens` (`id`, `url`, `titulo`, `data`) VALUES ";
        foreach(array_reverse($a) as $image) {
            $b .= "(NULL, '" . $image["href"] . "', '" . $image["title"] . "', '". $image["data"] ."-01') ,";
        }

        return $b;
    }
}
