<?php

namespace App\Http\Middleware;

use Menu;
use Closure;


class MenuAdmin
{
    public function handle($request, Closure $next)
    {
        // eu não preciso processar informações do menu se eu estiver fora de uma rota de admin
        if ($request->is('admin') || $request->is('admin/*')) {
            Menu::make('MenuPrincipal', function($menu){
                $menu->add('Dashboard', array('route' => 'admin.dashboard'))->data('icon', 'fa fa-bar-chart');

                $menu->add('Postagens')->data('icon', 'fa fa-file-text');
                $menu->postagens->add('Cadastrar Novo', array('route' => 'admin.posts.cadastrar'));
                $menu->postagens->add('Listagem', array('route' => 'admin.posts.listar'));

                $menu->add('Fotos', array('route' => 'admin.imagens.listar'))->data('icon', 'fa fa-picture-o');
            });
        }

        return $next($request);
    }
}
