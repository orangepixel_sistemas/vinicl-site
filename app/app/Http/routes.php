<?php
use App\Http\Controllers\SiteController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ImagensController;


// rotas do site
Route::get('/', ['as'=>'index', 'uses'=>'SiteController@index']);
Route::post('/email', ['as'=>'email', 'uses'=>'SiteController@email']);

Route::get('/teste', ['as'=>'teste', 'uses'=>'SiteController@teste']);


//rotas de autenticação
Route::get('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLogin']);
Route::post('login', ['as'=>'auth.login_ajax', 'uses'=>'Auth\AuthController@postLogin']);
Route::get('logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);


// rotas do painel admin
Route::group(['prefix' => 'admin', 'as'=>'admin.', 'middleware'=>'auth'], function () {
	Route::get('/logout', ['as'=>'auth.logout', 'uses'=>'Auth\AuthController@logout']);
	Route::get('/', ['as'=>'dashboard', 'uses'=>'AdminController@index']);

	Route::get('/dashboard', ['as'=>'dashboard', 'uses'=>'AdminController@index']);

	//rotas dos posts
	Route::group(['prefix' => 'posts', 'as'=>'posts.'], function () {
		//rotas do painel
		Route::get('/listar', ['as'=>'listar', 'uses'=>'AdminController@postsShow']);
		Route::get('/cadastrar', ['as'=>'cadastrar', 'uses'=>'AdminController@postsCreate']);
		Route::get('/editar/{id?}', ['as'=>'editar', 'uses'=>'AdminController@postsEdit']);

		//rotas da API
		Route::post('/', ['as'=>'salvar', 'uses'=>'PostsController@store']);
		Route::get('/', ['as' => 'listar_dt', 'uses'=>'PostsController@datatables_ajax']);
		Route::put('/editar/{id?}', ['as'=>'editar', 'uses'=>'PostsController@update']);
		Route::delete('/{id?}', ['as'=>'deletar', 'uses'=>'PostsController@destroy']);

		Route::put('/editar_ordenacao', ['as'=>'editar_ordenacao', 'uses'=>'PostsController@updateOrdenacao']);
	});

	Route::group(['prefix' => 'imagens', 'as'=>'imagens.'], function () {
		//rotas do painel
		Route::get('/', ['as'=>'listar', 'uses'=>'AdminController@imagensShow']);

		//rotas da API
		Route::post('/', ['as'=>'dz_action', 'uses'=>'ImagensController@DropzoneAction']);
		Route::put('/editar/{id?}', ['as'=>'editar', 'uses'=>'ImagensController@update']);
		Route::delete('/{id?}', ['as'=>'deletar', 'uses'=>'ImagensController@destroy']);

		Route::get('/html_lista', ['as'=>'html_lista', 'uses'=>'ImagensController@html_lista']);
	});
});