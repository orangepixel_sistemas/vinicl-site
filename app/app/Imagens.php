<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagens extends Model
{
    public $timestamps = false;

	protected $guarded = array('id');
	protected $fillable = array('url', 'titulo', 'data');
}
