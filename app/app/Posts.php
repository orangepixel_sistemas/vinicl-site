<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    public $timestamps = true;

	protected $guarded = array('id');
	protected $fillable = array('id_usuario', 'mostrar_pagina_inicial', 'slug', 'ordem', 'titulo', 'conteudo');

	public static $rules = array(
		'titulo' 				=> 'required|min: 5',
		'conteudo' 				=> 'required'
	);

}
