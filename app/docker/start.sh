#!/bin/sh

# Tweak nginx to match the workers to cpu's
procs=$(cat /proc/cpuinfo | grep processor | wc -l)
sed -i -e "s/worker_processes 4/worker_processes $procs/" /etc/nginx/nginx.conf

# nginx pid
mkdir -p /run/nginx
touch /run/nginx/nginx.pid

# composer
#composer install

# chown
#chown -R www:www /app

# migrate
#php artisan migrate --force

# lumen schedules for crond
crontab /app/docker/lumen-schedules.txt

# Start supervisord and services
supervisord -n -c /app/docker/supervisord.conf