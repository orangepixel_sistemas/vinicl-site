-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11-Nov-2016 às 16:06
-- Versão do servidor: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vinicius_site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `imagens`
--

CREATE TABLE `imagens` (
  `id` int(11) NOT NULL,
  `url` varchar(250) NOT NULL,
  `titulo` varchar(64) NOT NULL,
  `data` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `imagens`
--

INSERT INTO `imagens` (`id`, `url`, `titulo`, `data`) VALUES
(74, 'uploads/24.jpg', 'I Workshop de Álgebra e Topologia Algébrica da Bahia', '2016-02-01'),
(73, 'uploads/1.jpg', 'Defesa de Doutorado', '2015-07-01'),
(72, 'uploads/2.jpg', 'Defesa de Doutorado', '2015-07-01'),
(71, 'uploads/3.jpg', 'Defesa de Doutorado', '2015-07-01'),
(70, 'uploads/4.jpg', 'Winter Braids V - Pau, França', '2015-02-01'),
(69, 'uploads/5.jpg', 'I Encontro de Álgebra e Topologia Algébrica da UFSCAr, São Carlo', '2015-02-01'),
(68, 'uploads/6.jpg', 'Feriado de um matemático', '2014-08-01'),
(67, 'uploads/8.jpg', 'XIX Encontro Brasileiro de Topologia, São José do Rio Preto SP', '2014-08-01'),
(66, 'uploads/7.jpg', 'XIX Encontro Brasileiro de Topologia - Palestra', '2014-08-01'),
(65, 'uploads/9.jpg', 'Trem Bala Zurique - Paris', '2014-05-01'),
(64, 'uploads/10.jpg', 'Trem Bala Zurique - Paris', '2014-05-01'),
(63, 'uploads/11.jpg', 'Trem Bala Zurique - Paris', '2014-05-01'),
(62, 'uploads/12.jpg', 'Trem Bala Zurique - Paris', '2014-05-01'),
(61, 'uploads/14.jpg', 'Université de Caen, França', '2014-03-01'),
(60, 'uploads/15.jpg', 'Université de Caen, França - Tranway', '2014-03-01'),
(59, 'uploads/13.jpg', 'Université de Caen, França - Comida Boa =P', '2014-03-01'),
(58, 'uploads/16.jpg', 'XVIII Encontro Brasileiro de Topologia, Águas de Lindoia SP', '2012-08-01'),
(57, 'uploads/17.jpg', 'Defesa de Mestrado', '2011-02-01'),
(56, 'uploads/18.jpg', 'Defesa de Mestrado - Arguição da Banca', '2011-02-01'),
(55, 'uploads/19.jpg', 'Defesa de Mestrado - Turma boa', '2011-02-01'),
(54, 'uploads/20.jpg', 'Simpósio de Topologia Algébrica, Brotas SP', '2010-10-01'),
(53, 'uploads/21.jpg', 'XVII Encontro Brasileiro de Topologia, Rio de Janeiro RJ', '2010-08-01'),
(52, 'uploads/22.jpg', 'Colóquio Brasileiro de Matemática, Rio de Janeiro RJ', '2009-07-01'),
(51, 'uploads/23.jpg', 'Colóquio Brasileiro de Matemática - Bons papos', '2009-07-01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `imagens`
--
ALTER TABLE `imagens`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `imagens`
--
ALTER TABLE `imagens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
