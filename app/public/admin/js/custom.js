$("body").tooltip({
    selector: '[data-toggle="tooltip"]'
});

jQuery.extend(jQuery.validator.messages, {
	required: "Este campo &eacute; requerido.",
	remote: "Por favor, corrija este campo.",
	email: "Por favor, forne&ccedil;a um endere&ccedil;o eletr&ocirc;nico v&aacute;lido.",
	url: "Por favor, forne&ccedil;a uma URL v&aacute;lida.",
	date: "Por favor, forne&ccedil;a uma data v&aacute;lida.",
	dateISO: "Por favor, forne&ccedil;a uma data v&aacute;lida (ISO).",
	number: "Por favor, forne&ccedil;a um n&uacute;mero v&aacute;lido.",
	digits: "Por favor, forne&ccedil;a somente d&iacute;gitos.",
	creditcard: "Por favor, forne&ccedil;a um cart&atilde;o de cr&eacute;dito v&aacute;lido.",
	equalTo: "Por favor, forne&ccedil;a o mesmo valor novamente.",
	accept: "Por favor, forne&ccedil;a um valor com uma extens&atilde;o v&aacute;lida.",
	maxlength: jQuery.validator.format("Por favor, forne&ccedil;a n&atilde;o mais que {0} caracteres."),
	minlength: jQuery.validator.format("Por favor, forne&ccedil;a ao menos {0} caracteres."),
	rangelength: jQuery.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1} caracteres de comprimento."),
	range: jQuery.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1}."),
	max: jQuery.validator.format("Por favor, forne&ccedil;a um valor menor ou igual a {0}."),
	min: jQuery.validator.format("Por favor, forne&ccedil;a um valor maior ou igual a {0}.")
});

jQuery.validator.setDefaults({
    success: "valid",
    errorClass: "form-error",
    validClass: "form-success",
    errorElement: "div",
    rules: {       
        avatar: {extension:"jpg|png|gif|jpeg|doc|docx|pdf|xls|rar|zip"},
        password2: {equalTo: '#password'},
        calcul: {operation: 12},
        url: {url: true},
        email: {
            required:  {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }   
                },
            customemail: true
        },
    },
    highlight: function(element, errorClass, validClass) {
        $(element).closest('.form-control').addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.form-control').removeClass(errorClass).addClass(validClass);
    },
    errorPlacement: function(error, element) {
       if (element.hasClass("custom-file") || element.hasClass("checkbox-type") || element.hasClass("language")) {
            element.closest('.option-group').after(error);
       }
       else if (element.is(":radio") || element.is(":checkbox"))  {
            element.closest('.option-group').after(error);
       }
       else if (element.parent().hasClass('input-group'))  {
            element.parent().after(error);
       }
       else{
           error.insertAfter(element);
       }
    },
    invalidHandler: function(event, validator) {
        var errors = validator.numberOfInvalids();         
    }      
});


function generateNoty(position, type, title, content) {
    method = 3000;
    style = "made";
    content2 = '<div class="alert alert-' + type + ' media fade in">';
    content2 += '<h4 class="alert-title">' + title + '</h4>';
    content2 += '<p>' + content + '</p>';
    content2 += '</div>';

    content = content2;

    // console.log('position: '+ position + ',container: '+ container + ', style: ' + style);
    if(position == 'bottom') {
        openAnimation = 'animated fadeInUp';
        closeAnimation = 'animated fadeOutDown';
    } else if(position == 'top') {
        openAnimation = 'animated fadeIn';
        closeAnimation = 'animated fadeOut';
    } else {
        openAnimation = 'animated bounceIn';
        closeAnimation = 'animated bounceOut';
    }

    var n = noty({
            text : content,
            type : type,
            dismissQueue: true,
            layout : position,
            closeWith : ['click'],
            theme : 'made',
            maxVisible: 10,
            animation   : {
            open : openAnimation,
            close : closeAnimation,
            easing : 'swing',
            speed : 100
        },
        timeout: method            
    });
}
