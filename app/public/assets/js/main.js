//Use Strict Mode
(function($) {
  "use strict";

  $(window).load(function() {
      setTimeout(function() {
        updateImagesSRC();
      }, 1000);

    function updateImagesSRC() {
      console.log("TRIGGERED");
      $(".project-thumbnail").each(function(i, obj) {
        var a = $(obj).attr("style", "background-image: " + $(obj).attr("data-src") + " !important");
        console.log(a);
      });

      $('a.nivobox').nivoLightbox({
        effect: 'fade'
      });
    }
  });

  //Begin - Window Load
  $(window).load(function() {

    //==============___Page Loader___================

    $('#page-loader').delay(300).fadeOut(400, function() {

    });

    $('#loader-name').addClass('loader-left');
    $('#loader-job').addClass('loader-right');
    $('#loader-animation').addClass('loader-hide');

  });

  //Begin - Document Ready
  $(document).ready(function() {

    //==============___Page Loader___================
    $('#loading-wraper').fadeIn(300);

    //==============___Scrollbars___================
    $('.section-vcardbody').perfectScrollbar({
      wheelSpeed: 0.9
    });

    //==============___Menu & Pages Animation___================

    var linkHome = 0;
    var linkPage = '';

    function pageOn() {
      $('#main-menu').addClass('main-menu-pgactive');
      $('#section-home').addClass('section-vcardbody-pgactive');
      $('.profileActive').removeClass('profileActive');
      $('#profile2').addClass('profileActive');

      linkHome = 1;
    }

    function pageOff() {
      $('.section-page-active').removeClass('section-page-active');
      $('#main-menu').removeClass('main-menu-pgactive');
      $('#section-home').removeClass('section-vcardbody-pgactive');
      $('.profileActive').removeClass('profileActive');
      $('#profile1').addClass('profileActive');
      linkHome = 0;
    }

    $(".link-page").on('click', function(event) {
      $("#24").trigger('click');
      event.preventDefault();
      $('.menuActive').removeClass('menuActive');
      $(this).addClass('menuActive');
      linkPage = $(this).attr('href');
      $('.section-page-active').removeClass('section-page-active');
      $(linkPage).addClass('section-page-active');
      pageOn();
    });

    $(".link-home").on('click', function(event) {
      event.preventDefault();

      if (linkHome == 0) {
        //pageOn();
      } else if (linkHome == 1) {
        $('.menuActive').removeClass('menuActive');
        $(this).addClass('menuActive');
        pageOff();
      }
    });

    //==============___Blog - Ajax___================
    function loadPost() {
      $.ajax({
        url: 'single.html', // URL HERE
        type: 'GET',
        success: function(html) {

          var $lis = $(html).find('#blogPost'); // Loads the content inside #blogPost div

          $("#postHere").html($lis);
        }
      });
    }

    $(".loadPost").on('click', function(event) {
      event.preventDefault();
      //$("#postHere").html('loading...');
      $('.section-page-active').removeClass('section-page-active');
      $('#page-blog-single').addClass('section-page-active');
      pageOn();
      loadPost();
    });

    //==============___Contact Form Validator and Ajax Sender___================
    $("#contactForm").validate({
      submitHandler: function(form) {
        console.log($("#contactForm").serialize() + "&_token=" + $("[name=_token]").val());
        $.ajax({
          type: "POST",
          url: email_route,
          data: $("#contactForm").serialize() + "&_token=" + $("[name=_token]").val(),
          dataType: "json",
          success: function(data) {
            console.log(data);
            if (data.response == null) {
              $("#contactSuccess").fadeIn(300);
              $("#contactError").addClass("hidden");

              $("#contactForm #name, #contactForm #email, #contactForm #subject, #contactForm #message")
                .val("")
                .blur()
                .closest(".control-group")
                .removeClass("success")
                .removeClass("error");
            } else {
              $("#contactError").fadeIn(300);
              $("#contactSuccess").addClass("hidden");
            }
          },
          error: function(data) {
            console.log(data);
          }

        });
      }
    });

    //Modal for Contact Form
    $('.modal-wrap').click(function() {
      $('.modal-wrap').fadeOut(300);
    });

    //jquery validation
jQuery.extend(jQuery.validator.messages, {
    required: "Este campo &eacute; requerido.",
    remote: "Por favor, corrija este campo.",
    email: "Por favor, forne&ccedil;a um endere&ccedil;o eletr&ocirc;nico v&aacute;lido.",
    url: "Por favor, forne&ccedil;a uma URL v&aacute;lida.",
    date: "Por favor, forne&ccedil;a uma data v&aacute;lida.",
    dateISO: "Por favor, forne&ccedil;a uma data v&aacute;lida (ISO).",
    number: "Por favor, forne&ccedil;a um n&uacute;mero v&aacute;lido.",
    digits: "Por favor, forne&ccedil;a somente d&iacute;gitos.",
    creditcard: "Por favor, forne&ccedil;a um cart&atilde;o de cr&eacute;dito v&aacute;lido.",
    equalTo: "Por favor, forne&ccedil;a o mesmo valor novamente.",
    accept: "Por favor, forne&ccedil;a um valor com uma extens&atilde;o v&aacute;lida.",
    maxlength: jQuery.validator.format("Por favor, forne&ccedil;a n&atilde;o mais que {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, forne&ccedil;a ao menos {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1} caracteres de comprimento."),
    range: jQuery.validator.format("Por favor, forne&ccedil;a um valor entre {0} e {1}."),
    max: jQuery.validator.format("Por favor, forne&ccedil;a um valor menor ou igual a {0}."),
    min: jQuery.validator.format("Por favor, forne&ccedil;a um valor maior ou igual a {0}.")
});

jQuery.validator.setDefaults({
    success: "valid",
    errorElement: "span",
    errorClass: "help-block",
    rules: {       
        avatar: {extension:"jpg|png|gif|jpeg|doc|docx|pdf|xls|rar|zip"},
        password2: {equalTo: '#password'},
        calcul: {operation: 12},
        url: {url: true},
        email: {
            required:  {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }   
                },
            customemail: true
        },
    },
    highlight: function (element, errorClass, validClass) {
        if (element.type === "radio") {
            this.findByName(element.name).addClass(errorClass).removeClass(validClass);
        } else {
            $(element).closest('.form-group').removeClass('has-success has-feedback').addClass('has-error has-feedback');
            $(element).closest('.form-group').find('span.glyphicon').remove();
            $(element).closest('.form-group').append('<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>');
        }
    },
    unhighlight: function (element, errorClass, validClass) {
        if (element.type === "radio") {
            this.findByName(element.name).removeClass(errorClass).addClass(validClass);
        } else {
            $(element).closest('.form-group').removeClass('has-error has-feedback').addClass('has-success has-feedback');
            $(element).closest('.form-group').find('span.glyphicon').remove();
            $(element).closest('.form-group').append('<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>');
        }
    },
    invalidHandler: function(event, validator) {
        var errors = validator.numberOfInvalids();         
    }      
});

    //End - Document Ready
  });

  //End - Use Strict mode
})(jQuery);