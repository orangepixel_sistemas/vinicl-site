@extends("admin.layouts.default")

@section("titulo", "Dashboard")
@section("descricao", 'Página inicial')

@section("conteudo")
<div class="row">
	<div class="col-md-4">
		<div class="card">
			<div class="card-content">
				<span class="card-title">Páginas / Postagens</span>
				<p>Para editar basta utilizar o menu ao lado ou os botões abaixo.</p>
			</div>
			<div class="card-action">
				<a href="{{ route('admin.posts.cadastrar') }}">Cadastrar Novo</a>
				<a href="{{ route('admin.posts.listar') }}">Editar/Deletar</a>
			</div>
		</div>
	</div>
</div>
@endsection