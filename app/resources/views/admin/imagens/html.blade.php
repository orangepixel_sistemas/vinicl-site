<div style="margin-bottom: 30px">
	Mostrar com: 
	<select id="organizaImagens" class="form-control hidden-xs" style="width: 130px; display: inline">
		<option value="2">2 colunas</option>
		<option value="3" selected="selected">3 colunas</option>
		<option value="4">4 colunas</option>
		<option value="6">6 colunas</option>
	</select><br />
</div>


<div class="row" id="imagens2">
@foreach($imagens as $img)
	<div class="col-sm-6 col-md-4" id="imagem-{{ $img->id }}">
		<div class="card small">
			<div class="card-image" style="height: 250px">
				<img class="activator img-datasrc" data-src="{!! asset($img->url) !!}">
			</div>
			<div class="card-content" style="min-height: 100px">
				<span class="card-title activator grey-text text-darken-4" style="max-width: calc(100% - 26px); float:left; margin: 0">{!! $img->titulo !!}</span>
				<i class="mdi-navigation-more-vert activator f-right" style="cursor: pointer"></i>
			</div>
			<div class="card-reveal">
				<span class="card-title grey-text text-darken-4">{!! $img->titulo !!} - <small>(ID: <b>{{ $img->id }}</b>)</small> <i class="mdi-navigation-close f-right"></i></span>
				<p>
					<div class="form-group">
						<label class="control-label">Titulo</label>
						<input type="text" id="titulo-{{ $img->id }}" class="form-control" name="titulo" value="{!! $img->titulo !!}">
					</div>

					<div class="form-group">
						<label class="control-label">Data</label>
						<input type="text" id="data-{{ $img->id }}" class="form-control datemask" name="data" value="{!! date('d-m-Y', strtotime($img->data)) !!}">
					</div>

					<a href="#" class="btn btn-success btn-block" onClick="editarImagem('{{ $img->id }}'); return false;" style="margin: 10px 0 10px 0">
						<i class="fa fa-pencil-square-o"></i> Editar Informações
					</a>

					<a target="_blank" href="{!! asset($img->url) !!}" class="btn btn-primary btn-block" style="margin: 10px 0 10px 0">
						<i class="fa fa-eye"></i> Visualizar Imagem
					</a>

					<a href="#imagens" class="btn btn-default btn-block" onclick="modalCropSkuImage('{!! $img->url !!}', '');" style="margin: 10px 0 10px 0" disabled="disabled">
						<i class="fa fa-scissors"></i> Cortar Imagem (em breve)
					</a>

					<a href="#imagens" class="btn btn-danger btn-block" onClick="removerImagem('{{ $img->id }}'); return false;"><i class="fa fa-times"></i> Remover Imagem</a>
				</p>
			</div>
		</div>
	</div>
@endforeach
</div>