@extends("admin.layouts.default")

@section("titulo", "Cadastrar Novo Post")

@section("conteudo")
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default no-bd">
			<div class="panel-header bg-dark">
				<h2 class="panel-title">
					<strong>Enviar</strong> imagens
				</h2>
			</div>
			<div class="panel-body bg-white">
				<div class="row">
					<div class="col-md-12">
						<div id="dZUpload" class="dropzone">
							<div class="dz-default dz-message"></div>
						</div><br />
						<a href="#imagens" id="sendImagensDropzone" class="btn btn-success btn-lg btn-block"><i class="fa fa-upload"></i> Enviar Imagens</a>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xs-12">
		<div class="panel panel-default no-bd">
			<div class="panel-header bg-dark">
				<h2 class="panel-title">
					<strong>Editar</strong> imagens
				</h2>
			</div>
			<div class="panel-body bg-white">
				<div class="row">
					<div class="col-md-12">
						<div id="imagensArea">
							@if(count($imagens) > 0)
								@include('admin.imagens.html', $imagens)
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- modal de croppar -->
<div class="modal fade" id="modalCropar" role="dialog" aria-labelledby="modalLabel" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
				<h4 class="modal-title" id="modalLabel">Cortar Imagem <small id="imageModalCropparID">(ID: )</small></h4>
			</div>
			<div class="modal-body" style="text-align: center">
				<div class="img-container">
					<img id="imageModalCroppar" alt="Imagem Croppar" style="max-width: 100%;">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-success" id="modalCropparAction" data-dismiss="modal"><i class="fa fa-scissors"></i> Cortar e Salvar</button>
			</div>
		</div>
	</div>
</div>

<div id="preview-template" style="display: none;">
	<div class="dz-preview dz-file-preview">
		<div class="dz-image"><img data-dz-thumbnail=""></div>

		<div class="dz-details">
			<div class="dz-size"><span data-dz-size=""></span></div>
			<div class="dz-filename-input">Titulo: <br /><input type="text" class="form-control" placeholder="Insira o Titulo da Foto"></div>
			<div class="dz-data-input">Data: <br /><input type="text" class="form-control datemask" placeholder="DD/MM/YYYY"></div>
			<!--<div class="dz-filename"><span data-dz-name=""></span></div>-->
		</div>
		<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
		<div class="dz-error-message"><span data-dz-errormessage=""></span></div>

		<div class="dz-success-mark">
			<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
				<!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
				<title>Check</title>
				<desc>Created with Sketch.</desc>
				<defs></defs>
				<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
					<path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
				</g>
			</svg>
		</div>

		<div class="dz-error-mark">
			<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
				<!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
				<title>error</title>
				<desc>Created with Sketch.</desc>
				<defs></defs>
				<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
					<g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
						<path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
					</g>
				</g>
			</svg>
		</div>

	</div>
</div>
@endsection

@section("extra-css")
	<!-- dropzone -->
	<link rel="stylesheet" href="{!! asset('admin/plugins/dropzone/css/dropzone.css') !!}">
	<style>
		.card .card-image img {
			background-position-y: 50%;
			background-position-x: 50%;
			background-size: 100%;
			height: 250px;
			width: 100%;
		}

		.dropzone .dz-preview.dz-file-preview [data-dz-thumbnail], 
		.dropzone-previews .dz-preview.dz-file-preview [data-dz-thumbnail] {
		  width: auto;
		  min-width: 100px;
		  height: 100px;
		  display: inline !important;
		}

		.dropzone .dz-preview .dz-details, .dropzone-previews .dz-preview .dz-details {
		  width: auto !important;
		  height: auto !important;
		  max-height: 120px !important;
		}

		@media (-webkit-min-device-pixel-ratio: 1.5), not all, not all, not all, (min-resolution: 138dpi), (min-resolution: 1.5dppx) {
		  .dropzone .dz-default.dz-message {
			background-size: 75% !important;
		   background-position: 50px 30px !important;
		  }
		}		
	</style>
@endsection

@section("extra-js")
	<!-- dropzone -->
	<script src="{!! asset('admin/plugins/dropzone/js/dropzone.js') !!}"></script>
	<script src="{!! asset('admin/plugins/input-mask/jquery.maskedinput.min.js') !!}"></script>
	<script src="{!! asset('admin/plugins/input-mask/jquery.inputmask.js') !!}"></script>

	<!--	<script src="{!! asset('dmin/js/cadastro_dropzone_nocrop.js') !!}"></script>
	<script src="{!! asset('mediaadmin/js/cadastro_cropper.js') !!}"></script>-->

	<!-- excluir imagem -->
	<script type="text/javascript">
		$(window).load(function() {
			updateImagesSRC();
		});

		$(".datemask").inputmask("99/99/9999");

		function updateImagesSRC() {
			$(".img-datasrc").each(function(i, obj) {
				$(obj).css("background-image", "url(" + $(obj).attr("data-src") + ")");
			});
		}

		function removerImagem(id) {
			generateNoty("topRight", "info", "Excluindo imagem...", "Por favor, aguarde um instante", 2000);
			$.ajax({
				type: 'delete',
				url: '{{ route('admin.imagens.deletar') }}/'+id,
				headers: { "Accept-Language": "application/json; charset=utf-8", },
				success: function(data) {
					generateNoty("topRight", "success", "Imagem excluída com sucesso", '', 4000);
					$("#imagem-"+id).hide();
				},

				error: function(data) {
					// é uma string, provavelmente um json
					var msgs = "";
					$.each(data.responseJSON, function(i, item) {
						msgs += item + "<br>\n";
					});
					generateNoty("topRight", "danger", "Erro ao excluir imagem:", msgs, 15000);
					console.log(data);
				}
			});

			event.preventDefault();
		}

		function editarImagem(id) {
			generateNoty("topRight", "info", "Editando imagem...", "Por favor, aguarde um instante", 2000);
			$.ajax({
				type: 'put',
				url: '{{ route('admin.imagens.editar') }}/'+id,
				data: {"titulo": $("#titulo-"+id).val(), "data": $("#data-"+id).val()},
				headers: { "Accept-Language": "application/json; charset=utf-8", },
				success: function(data) {
					generateNoty("topRight", "success", "Imagem editada com sucesso", '', 4000);
				},

				error: function(data) {
					// é uma string, provavelmente um json
					var msgs = "";
					$.each(data.responseJSON, function(i, item) {
						msgs += item + "<br>\n";
					});
					generateNoty("topRight", "danger", "Erro ao editar imagem:", msgs, 15000);
					console.log(data);
				}
			});

			event.preventDefault();
		}

		function updateImagesHTML() {			
			$.ajax({
				type: 'get',
				url: '{{ route('admin.imagens.html_lista') }}',
				success: function(data) {
					$("#imagensArea").html(data);
					updateImagesSRC();
				},

				error: function(data) {
					// é uma string, provavelmente um json
					var msgs = "";
					$.each(data.responseJSON, function(i, item) {
						msgs += item + "<br>\n";
					});
					generateNoty("topRight", "danger", "Erro ao pegar imagens do banco:", msgs, 15000);
					console.log(data);
				}
			});
		};

		$("body").on("change", "#organizaImagens", function() {
			$('#imagens2').children("*").removeClass();
			$('#imagens2').children("*").addClass("col-sm-" + 12/$("#organizaImagens").val());
		}); 
	



		function dataURLtoBlob(dataurl) {
			var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
				bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
			while(n--){
				u8arr[n] = bstr.charCodeAt(n);
			}
			return new Blob([u8arr], {type:mime});
		}

		Dropzone.autoDiscover = false;
		var upload_max_file_size ='{!! str_replace('M', '', ini_get('upload_max_filesize')) !!}';

		var myDropzone = new Dropzone('#dZUpload', {
			url: "{{ route('admin.imagens.dz_action') }}",
			//headers: $("#formularioDropzone").serialize(),
			headers: {
				'X-CSRF-Token': $('input[name="_token"]').val()
			},
			responsive: true,
			autoProcessQueue: false,
			addRemoveLinks: true,
			uploadMultiple: false,
			parallelUploads: 35,
			acceptedFiles: "image/jpeg,image/png,image/gif",
			maxFiles: 26,
			maxThumbnailFilesize: 100,
			thumbnailWidth: '1024',
			thumbnailHeight: null,
			previewsContainer: '#dZUpload',
			previewTemplate: $('#preview-template').html(),

			thumbnail: function(file, dataURL) {
				$this = this;

				if(file.processed) {
					if(file.size/1000000 > upload_max_file_size) {
						$this.removeFile(file);
					}
					return ;
				}

				$this.removeFile(file);

				var newFile = dataURLtoBlob(dataURL);
				newFile.name = file.name;
				newFile.extension = file.name.substring(file.name.lastIndexOf('.')+1);

				newFile.processed = true;

				myDropzone.addFile(newFile);
				$(newFile.previewElement).children(".dz-image").children("img").attr("src", dataURL);

				if(newFile.size/1000000 > upload_max_file_size) {
					$this.removeFile(newFile);
				}
			},

			init: function() {
				this.on("addedfile", function(file) {
					$(".datemask").inputmask("99/99/9999");
					n = file.name.lastIndexOf(".");
					nome = n > -1 ? file.name.substr(0, n) : file.name;

					$(file.previewElement).children(".dz-details").children(".dz-filename-input").
					children("input").attr("value", nome);
				});

				this.on("sending", function(file, xhr, formData) {
					titulo = $(file.previewElement).
						children(".dz-details").
						children(".dz-filename-input").
						children("input").
						val() + "." + file.extension;

					data = $(file.previewElement).
						children(".dz-details").
						children(".dz-data-input").
						children("input").
						val();	

					formData.append('titulo', titulo);
					formData.append('data', data);
				});
			},

			success: function(file, data) {
				//console.log(file);
				myDropzone.removeFile(file);
				generateNoty("topRight", "success", "Imagem salva com sucesso!", 'O arquivo <b>' + file.name + '</b> foi enviado com sucesso!', 4000);

				//unblockUI('#dZUpload');
				//console.log(data);
			   	updateImagesHTML();
			},

			error: function(file, errorMessage, xhr) {
				//unblockUI('#dZUpload');

				if(errorMessage) {
					generateNoty("topRight", "danger", "Ocorreu um erro!", "Não foi possível enviar o arquivo <b>" + file.name + "</b>.<br />" + errorMessage, 15000);
				} else {
					generateNoty("topRight", "danger", "Ocorreu um erro!", "Não foi possível enviar o arquivo <b>" + file.name + "</b>.<br />Tente enviar o arquivo novamente.", 15000);
				}
			}
		});

		$("#sendImagensDropzone").on("click", function() {
			myDropzone.processQueue();
		});

	</script>
@endsection