<!DOCTYPE html>
<html class="" lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

		<title>OrangePixel - Painel Administrativo - @yield("titulo")</title>

		<link href="https://fonts.googleapis.com/css?family=Nothing+You+Could+Do" rel="stylesheet" type="text/css">

		<link href="{!! asset('admin/css/style.css') !!}" rel="stylesheet">
		<link href="{!! asset('admin/css/theme.css') !!}" rel="stylesheet">
		<link href="{!! asset('admin/css/ui.css') !!}" rel="stylesheet">

		<link href="{!! asset('admin/material-design/css/material.css') !!}" rel="stylesheet">
		<link href="{!! asset('admin/css/layout.css') !!}" rel="stylesheet">

		@yield("extra-css")

		<!--[if lt IE 9]>
			<script src="{!! asset('admin/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js') !!}"></script>
		<![endif]-->
	</head>

	<body class="fixed-sidebar fixed-topbar color-default theme-sdtl">
		<section>
			<!-- BEGIN SIDEBAR -->
			<div class="sidebar">
				<div class="logopanel">
					<h1><a href="{!! route('index') !!}" style="background: url('{!! asset('admin/images/small-logo.png') !!}')">&nbsp;</a></h1>
				</div>
				<div class="sidebar-inner">
					<div class="sidebar-top"></div>
					<div class="menu-title">
						<span>Navega��o</span> 
						<div class="pull-right menu-settings">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300"> 
							<i class="icon-settings"></i>
							</a>
							<ul class="dropdown-menu">
								<li><a href="#" id="reorder-menu" class="reorder-menu">Reorder menu</a></li>
								<li><a href="#" id="remove-menu" class="remove-menu">Remove elements</a></li>
								<li><a href="#" id="hide-top-sidebar" class="hide-top-sidebar">Hide user &amp; search</a></li>
							</ul>
						</div>
					</div>
					@include('admin.layouts.menu', array('items' => $MenuPrincipal->roots()))
					
					<div class="sidebar-footer clearfix">
						<a class="pull-left footer-settings" href="#" data-rel="tooltip" data-placement="top" data-original-title="Settings">
							<i class="icon-settings"></i>
						</a>
						<a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top" data-original-title="Fullscreen">
							<i class="icon-size-fullscreen"></i>
						</a>
						<a class="pull-left" href="user-lockscreen.html" data-rel="tooltip" data-placement="top" data-original-title="Lockscreen">
							<i class="icon-lock"></i>
						</a>
						<a class="pull-left btn-effect" href="user-login-v1.html" data-modal="modal-1" data-rel="tooltip" data-placement="top" data-original-title="Logout">
							<i class="icon-power"></i>
						</a>
					</div>
				</div>
			</div>
			<!-- END SIDEBAR -->
			<div class="main-content">
				<!-- BEGIN TOPBAR -->
				<div class="topbar">
					<div class="header-left" style="display: block;">
						<div class="topnav">
							<a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>							
						</div>
					</div>					
				</div>

				<div class="page-content">
					@yield("conteudo")
				</div>
			</div>
		</section>

		<!-- Preloader -->
		<div class="loader-overlay">
			<div class="spinner">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
		</div>
		<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>


	</body>
		<script src="{!! asset('admin/plugins/jquery/jquery-1.11.1.min.js') !!}"></script>
		<!--<script src="{!! asset('admin/plugins/jquery/jquery-migrate-1.2.1.min.js') !!}"></script>-->
		<script src="{!! asset('admin/plugins/jquery-ui/jquery-ui-1.11.2.min.js') !!}"></script>

		<script src="{!! asset('admin/plugins/jquery-validation/jquery.validate.js') !!}"></script>
		<script src="{!! asset('admin/plugins/bootstrap/js/bootstrap.min.js') !!}"></script>

		<script src="{!! asset('admin/plugins/noty/jquery.noty.packaged.min.js') !!}"></script>

		<script src="{!! asset('admin/js/application.js') !!}"></script> <!-- Main Application Script -->
		<script src="{!! asset('admin/js/plugins.js') !!}"></script> <!-- Main Plugin Initialization Script -->
				
		<script src="{!! asset('admin/js/custom.js') !!}"></script> <!-- Main Plugin Initialization Script -->

		<script src="{!! asset('admin/material-design/js/material.js') !!}"></script>
		<script src="{!! asset('admin/js/layout.js') !!}"></script>

		<script>$.material.init();</script>
		@yield("extra-js")
</html>