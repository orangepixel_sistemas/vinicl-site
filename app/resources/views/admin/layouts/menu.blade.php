<ul class="nav nav-sidebar">
	@foreach($items as $item)
	<li @if(isset($item->attributes["class"]) and $item->hasChildren())
			class="nav-parent nav-active active"
		@elseif(!isset($item->attributes["class"]) and !$item->hasChildren())
			class="nonChildren"
		@elseif(isset($item->attributes["class"]) and !$item->hasChildren())
			class="nav-active active"
		@else
			class="nav-parent"
		@endif
		>
		<a href="{!! $item->url() !!}">
		@if($item->icon) <i class="{{ $item->icon }}"></i> @endif
			<span>{!! $item->title !!}</span>
			@if($item->hasChildren())
				<span class="fa arrow"></span>
			@endif
		</a>
		@if($item->hasChildren())
			<ul class="children collapse">					
				@foreach($item->children() as $item)
					<li
					@if(isset($item->attributes["class"]))
						class="active" 
					@endif>
						<a href="{!! $item->url() !!}">{!! $item->title !!}</a>
					</li>
				@endforeach
			</ul> 
		@endif
	</li>
	@endforeach
</ul>