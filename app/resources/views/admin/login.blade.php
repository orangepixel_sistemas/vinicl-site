<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>ESAC - Login no Administrativo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{!! asset('admin/css/font-awesome.min.css') !!}">
        <link href="{!! asset('admin/css/login.css') !!}" rel="stylesheet">
    </head>

    <body>
        <div class="wrapper">
        	<img src="{!! asset('admin/images/logo-painel.png') !!}" alt="OrangePixel Painel" style="max-width: 320px; margin-bottom: 20px">

            <!-- login normal -->
            <form class="login" role="form" method="POST" action="#">
                {!! csrf_field() !!}
                <p class="title">Login no Administrativo</p>
                <input type="text" name="email" value="{{ old('email') }}" placeholder="Email" autofocus/>
                <i class="fa fa-user"></i>
                <input type="password" placeholder="Senha" name="password" autocomplete="off">
                <i class="fa fa-key"></i>
                <a href="#" class="recovery-click">Recuperar minha senha</a>
                <button type="submit">
                    <i class="spinner"></i>
                    <span class="state">Logar</span>
                </button>
            </form>
            <!-- login normal -->

            <!-- recuperação de senha -->
            <form class="recovery" id="form_recovery" role="form" method="POST" action="#" style="display: none">
                {!! csrf_field() !!}

                <p class="title">Recuperaração de Senha</p>
                <input type="text" name="email" value="{{ old('email') }}" placeholder="Email" autofocus/>
                <i class="fa fa-user"></i>
                <a href="#" class="login-click">Fazer login normalmente</a>
                <button type="submit">
                    <i class="spinner"></i>
                    <span class="state">Enviar Email de Recuperação</span>
                </button>
            </form>
            <!-- recuperação de senha -->


            <div class="footer">
            	Desenvolvido por <a href="http://www.orangepixel.com.br/">OrangePixel.com.br</a><br />
            	<i class="fa fa-phone"></i> {{ env('ORANGE_TELEFONE') }}
            </div>
        </div>


    </body>
    <script src="{!! asset('assets/vendor/jquery.min.js') !!}"></script>

    <script type="text/javascript">
	$(".recovery-click").click(function() {
		$(".login").slideToggle("none");
		$(".recovery").slideToggle("easeInBounce");
	});

	$(".login-click").click(function() {
		$(".recovery").slideToggle("none");
		$(".login").slideToggle("easeInBounce");
	});


    var working = false;

    $('.login').on('submit', function(e) {
        e.preventDefault();
        if (working) return;
        working = true;
        var $this = $(this),
        $state = $(this).find('button > .state');
        $this.addClass('loading');
        $state.html('Autenticando');

        $.ajax({
            url: "{{ route('auth.login_ajax') }}",
            type: "post",
            data: {     
                'email':        $('input[name=email]').val(),
                'password':     $('input[name=password]').val(),
                '_token':       $('input[name=_token]').val()
            },
            
            success: function(data) {
                console.log(data);
                console.log("Authentication: " + data["auth"])

                if(data["auth"] == true) {
                	$this.addClass('ok');
                    $state.html('Olá! Seja bem vindo novamente.');
                    setTimeout(function() {
                        $(location).attr('href', "admin/dashboard")
                    }, 1000);
                    
                } else {
                    setTimeout(function() {
                        $this.addClass('error');
                        $state.html('O email ou senha estão incorretos. Tente novamente!');

                        $(".spinner").css("display", "none");
                        setTimeout(function() {
                            $state.html('Login');
                            $this.removeClass('error loading');
                            $(".spinner").css("display", "block");
                            working = false;
                        }, 3000);
                    }, 200);
                }
            },
            error: function() {
            	setTimeout(function() {
                    $this.addClass('error');
                    $state.html('O servidor está indisponível. Tente novamente!');
                    setTimeout(function() {
                        $state.html('Login');
                        $this.removeClass('error loading');
                        working = false;
                    }, 3000);
                }, 200);
            },
            timeout: 5000
        });
    });
    </script>
</html>