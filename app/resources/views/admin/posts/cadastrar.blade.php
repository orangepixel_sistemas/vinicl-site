@extends("admin.layouts.default")

@section("titulo", "Cadastrar Novo Post")

@section("conteudo")
<div class="row">
	<div class="col-md-12">
		<form role="form" id="cadastrar-postagem" class="form-validation" novalidate="novalidate">
			{!! csrf_field() !!}
			@if(isset($post->id))
				<input type="hidden" name="id" value="{{ @$post->id }}">
			@endif
			<div class="panel panel-default no-bd">
				<div class="panel-header bg-dark">
					<h2 class="panel-title">
						@if(isset($post->id))
							<strong>Editar</strong> postagem <small>(ID: {{ @$post->id }})</small>
						@else
							<strong>Cadastrar</strong> nova postagem
						@endif
					</h2>
				</div>
				<div class="panel-body bg-white">
					<div class="row">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Titulo</label>
										<input type="text" name="titulo" class="form-control" value="{{ @$post->titulo }}" placeholder="Insira o titulo do post..." required="" aria-required="true">
									</div>

									<div class="form-group">
										<label class="control-label">Slug</label>
										<input type="text" name="slug" class="form-control" value="{{ @$post->slug }}">
									</div>
								</div>
								
								<div class="col-sm-12">
									<div class="form-group togglebutton togglebutton-material-blue">
										<label class="control-label" style="font-weight: 500">Mostar na Página Inicial</label>
										<label style="text-align: right; float: right">
											<input type="checkbox" value="1" @if(@$post->mostrar_pagina_inicial) checked="" @endif name="mostrar_pagina_inicial" class="md-checkbox">
										</label>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Conteúdo</label>
										<textarea name="conteudo" class="form-control" rows="10">{{ @$post->conteudo }}</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Cadastrar</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@section("extra-js")
<script src="{!! asset('admin/plugins/tinymce/tinymce.min.js') !!}"></script>
<script src="{!! asset('admin/plugins/noty/jquery.noty.packaged.min.js') !!}"></script>
<script type="text/javascript">
	tinymce.init({
		selector: 'textarea',
        theme: 'modern',
        skin: 'lightgray',
        content_style: "body{font-family: 'Montserrat', sans-serif;-webkit-font-smoothing: subpixel-antialiased;}.section-title{color:#021533;text-transform:uppercase;font-size:22px;line-height:26px;font-weight:700;margin-top:0;border-bottom:1px solid #021533;padding-bottom:10px;margin-bottom:40px}.section-title2{font-weight:700;color:#08aeac;font-size:20px;line-height:24px;letter-spacing:-1px;padding-left:15px;padding-top:5px;padding-bottom:5px;border-left:4px solid #08aeac}.resume-item{padding:15px;border:4px double #ccc;margin-bottom:15px}.section-item-title-1,.section-item-title-2{font-weight:700;color:#333;padding-left:0;line-height:22px;margin:0 0 8px;letter-spacing:-1px}.section-item-title-1{font-size:18px}.section-item-title-2{border-bottom:1px dotted #021533;padding-bottom:5px;font-size:15px}.graduation-time,.job{line-height:15px;font-style:italic;color:#555;font-size:10px;margin-top:5px;border-bottom:1px solid #ccc;padding-bottom:5px}.graduation-description p{font-size:12px}",
        content_css: 'https://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css',
        height: 200,
        noneditable_noneditable_class: 'fa',
        extended_valid_elements: 'span[*]',
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen importcss',
            'insertdatetime media table contextmenu paste code fontawesome noneditable'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fontawesome',
        content_css: '//www.tinymce.com/css/codepen.min.css',
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
	});

	$("#cadastrar-postagem").validate({		
		submitHandler: function (form) {
			event.preventDefault();

			$('input:checkbox').not(':checked').each(function() {
				$("#cadastrar-postagem").append("<input type='hidden' name='" + this.name + "' value='0' />");
			});

			generateNoty("topRight", "info", "Efetuando Cadastro!", "Por favor, aguarde um instante!");
			$.ajax({
				@if(isset($post->id))
					url: "{{ route('admin.posts.editar', $post->id) }}",
					type: "put",
				@else
					url: "{{ route('admin.posts.salvar') }}",
					type: "post",
				@endif

				data: $(form).serialize(),
				headers: {
					"Accept-Language": "application/json; charset=utf-8",
				},

				success: function(result, status, xhr) {
					generateNoty("topRight", "success", "Cadastro/Edição Efetuado com Sucesso!", result.message);
				},

				error: function(xhr, status, error) {
					if(xhr.status == 400) {
						response = xhr.responseJSON.message;
						var conteudo = "";
						for(var i = 0; i < response.length; i++) {
							conteudo += "[" + i + "] - " + response[i] + "<br />";
						}
					}

					generateNoty("topRight", "danger", "Cadastro/Edição não Efetuado!", conteudo);
				}
			});
			return false; 
		}
	});
</script>
@endsection