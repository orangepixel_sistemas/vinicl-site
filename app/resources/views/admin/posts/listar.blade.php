@extends("admin.layouts.default")

@section("titulo", "Cadastrar Novo Jogo")

@section("conteudo")
<div class="row">
	<div class="col-md-8">
		<div class="panel panel-default no-bd">
			<div class="panel-header bg-dark">
				<h2 class="panel-title"><strong>Listagem</strong> das Postagens/Páginas</h2>
			</div>
			<div class="panel-body bg-white">
				<table id="posts" class="table table-striped teste">
					<thead>
						<tr>
							<th>ID</th>
							<th>Titulo</th>							
							<th>Opções</th>
							<th>Ações</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="panel panel-default no-bd">
			<div class="panel-header bg-dark">
				<h2 class="panel-title"><strong>Ordenação</strong></h2>
			</div>
			<div class="panel-body bg-white">
				<ul class="posts-sortable">
					@foreach($posts as $post)
						<li id="{{ $post->id }}" class="bg-yellow" style="padding:5px; margin: 10px 0 10px 0">
							<i class="fa fa-folder-o"></i> {{ $post->titulo }} <small>(ID: <b>{{ $post->id }}</b>)</small>
						</li>
					@endforeach					
				</ul><br/>
				<a href="#" class="btn btn-primary btn-block btn-salvar-tree"><i class="fa fa-pencil-square-o"></i> Mudar Ordenação</a>
			</div>
		</div>
	</div>
</div>

<div id="modal_deletar" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-red">
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title"><i class="fa fa-trash"></i> <strong>Deletar</strong> Postagem?</h4>
			</div>
			<div class="modal-body" style="margin-top: 10px">
				<p>Tem certeza que deseja prosseguir? Não será possível restaurar essa postagem posteriormente!</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Descartar</button>
				<button type="button" class="btn btn-transparent btn-danger delete-user" data-dismiss="modal">Deletar</button>
				{!! csrf_field() !!}
				<input type="hidden" id="postagem-deletar">
			</div>
		</div>
	</div>
</div>
@endsection

@section("extra-css")
<link href="{!! asset('admin/plugins/datatables/datatables.min.css') !!}" rel="stylesheet">
<link href="{!! asset('admin/plugins/datatables/datatables.bootstrap.css') !!}" rel="stylesheet">
@endsection

@section("extra-js")
<script src="{!! asset('admin/plugins/datatables/jquery.datatables.min.js') !!}"></script>
<script src="{!! asset('admin/plugins/datatables/datatables.bootstrap.js') !!}"></script>
<script src="{!! asset('admin/plugins/html-sortable/html.sortable.min.js') !!}"></script>

<script type="text/javascript">
	var tabela = $("#posts").DataTable({
		processing: true,
		serverSide: true,
		ajax: "{!! route('admin.posts.listar_dt') !!}",
		columns: [
			{ data: 'id', name: 'id' },
			{ data: 'titulo', name: 'titulo' },
			{ data: 'user_name', name: 'user_name' }
		],
		columnDefs: [
			{
				render: function(data, type, row) {
					var content="";
					if(row.mostrar_pagina_inicial == true) {
						content += '<button type="button" class="btn btn-sm btn-blue" data-toggle="tooltip" data-placement="top"  data-original-title="Mostrando na Página Inicial"><i class="fa fa-paper-plane-o"></i></button> ';
					} else {
						content += '<button type="button" class="btn btn-sm btn-dark" data-toggle="tooltip" data-placement="top" data-original-title="Não Mostrando na Página Inicial"><i class="fa fa-paper-plane-o"></i></button> ';
					}
					return content;
				},
				targets: 2
			},
			{
				render: function(data, type, row) {
					return '<a href="#" id="' + row.id + '" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> Mostrar no Site</a> <a href="{!! route('admin.posts.editar') !!}/' + row.id + '" id="' + row.id + '" class="btn btn-blue btn-sm"><i class="fa fa-pencil"></i> Editar</a> <a href="#" id="' + row.id + '" class="btn btn-danger btn-sm btn-m-deletar" data-toggle="modal" data-target="#modal_deletar"><i class="fa fa-minus-square-o"></i> Deletar</a>';
				},
				targets: 3
			}
		],
		language: {
			"url": "{!! asset('admin/plugins/datatables/Portuguese-Brasil.json') !!}"
		}
	});

	$(document).on("click", ".btn-m-deletar", function() {
		$("#postagem-deletar").val($(this).context.id);
	});

	$(".delete-user").on("click", function() {
		generateNoty("topRight", "info", "Deletando Registro!", "Por favor, aguarde um instante!");
		$.ajax({
			url: $("#postagem-deletar").val(),
			type: "delete",
			data: { 
				'id': $("#postagem-deletar").val(),
				'_token': $('input[name=_token]').val()
			},
			headers: {
				"Accept-Language": "application/json; charset=utf-8",
			},

			success: function(result, status, xhr) {
				generateNoty("topRight", "success", "Registro deletado com Sucesso!", result.message);
				tabela.ajax.reload();
			},

			error: function(xhr, status, error) {
				if(xhr.status == 400) {
					response = xhr.responseJSON.message;
					var conteudo = "";
					for(var i = 0; i < response.length; i++) {
						conteudo += "[" + i + "] - " + response[i] + "<br />";
					}
				}

				generateNoty("topRight", "danger", "Registro não deletado!", conteudo);
			}
		});
	});

	var sortable = $('.posts-sortable').sortable();
	

	$(".btn-salvar-tree").on("click", function() {
		generateNoty("topRight", "info", "Atualizando Ordenação.", "Por favor, aguarde um instante!");

		$.ajax({
			url: "{{ route('admin.posts.editar_ordenacao') }}",
			type: "put",
			data: {"sortable": sortable.sortable("toArray"), '_token': $('input[name=_token]').val()},

			success: function(result, status, xhr) {
				console.log(result);
				generateNoty("topRight", "success", "Ordenação atualizada com Sucesso!", result.message);
				tabela.ajax.reload();
			},

			error: function(xhr, status, error) {
				if(xhr.status == 400) {
					response = xhr.responseJSON.message;
					var conteudo = "";
					for(var i = 0; i < response.length; i++) {
						conteudo += "[" + i + "] - " + response[i] + "<br />";
					}
				}

				generateNoty("topRight", "danger", "Ordenação não feita!", conteudo);
			}
		});
	});
</script>
@endsection