@extends("site.layouts.default")

@section("titulo", "Página Inicial")
@section("descricao", "Página Inicial")

@section("conteudo")
		<div class="content">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						@foreach ($posts->reverse() as $post)
							@if($post->mostrar_index)
								<div class="post">
									<div class="post-title">
										<h3 class="{{ $post->cor }}-border"><i class="fa fa-newspaper-o"></i> {{ $post->titulo }}</h3>
									</div>
									<div class="post-info">
										@if (!empty($post->imagem))
											<img src="{!! asset($post->imagem) !!}" class="img-responsive" alt="LOL ESAC">
										@endif
										<div class="post-content">
											{!! nl2br($post->conteudo) !!}
										</div>
									</div>
								</div>
							@endif
						@endforeach
					</div>

					<div class="col-md-4">
						<div class="post">
							<div class="post-title">
								<h3 class="red-border">Ultimos Jogos</h3>
							</div>
							<div class="post-info">
								<!--<ul class="last-games">
									<li>
										<div class="img">
											<img src="{!! asset('site/img/post/ex2.jpg') !!}" alt="" class="img-responsive">
										</div>
										<div class="info">
											<small><i class="fa fa-calendar"></i> Maio 23, 14:30</small>
											<a href="#" class="teams">Alpaca eSports x Calculoucos Team</a>
											<div class="score">3 x 2</div>
										</div>
									</li>

									<li>
										<div class="img">
											<img src="{!! asset('site/img/post/ex4.jpg') !!}" alt="" class="img-responsive">
										</div>
										<div class="info">
											<small><i class="fa fa-calendar"></i> Maio 25, 14:30</small>
											<a href="#" class="teams">Fledson Paranaue x Anthonynho</a>
											<div class="score">1 x 3</div>
										</div>
									</li>

									<li>
										<div class="img">
											<img src="{!! asset('site/img/post/ex3.jpg') !!}" alt="" class="img-responsive">
										</div>
										<div class="info">
											<small><i class="fa fa-calendar"></i> Maio 24, 14:30</small>
											<a href="#" class="teams">Natus Vincere x Alliance eSports</a>
											<div class="score">4 x 3</div>
										</div>
									</li>
								</ul>-->
								<div class="post-content">
									Nenhum jogo por enquanto.
								</div>
							</div>
						</div>
						<!--<div class="post">
							<div class="post-title">
								<h3 class="green-border">Nosso Newsletter</h3>
							</div>
							<div class="post-info">
								<div class="post-content">
									<form class="form">
										<div class="form-group">
											<label for="nome">SE INCREVA EM NOSSO NEWSLETTER:</label>
											<input type="nome" class="form-control" placeholder="Nome">
										</div>
										<div class="form-group">
											<input type="email" class="form-control" placeholder="Email">
										</div>
										<button type="submit" class="btn btn-info">CADASTRAR</button>
									</form>
								</div>
							</div>
						</div>-->
					</div>
				</div>

			</div>
		</div>
@endsection