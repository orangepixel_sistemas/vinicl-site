@extends("site.layouts.noslide")

@section("titulo", "{{ $post->titulo }}")

@section("conteudo")
		<div class="content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="post">
							<div class="post-title">
								<h3 class="{{ $post->cor }}-border"><i class="fa fa-newspaper-o"></i> {{ $post->titulo }}</h3>
							</div>
							<div class="post-info">
								@if (!empty($post->imagem))
									<img src="{!! asset($post->imagem) !!}" class="img-responsive" alt="LOL ESAC">
								@endif
								<div class="post-content">
									{!! $post->conteudo !!}
								</div>
							</div>
						</div>
					</div>

					
				</div>

			</div>
		</div>
@endsection