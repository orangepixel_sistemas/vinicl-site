@extends("site.layouts.default")

@section("conteudo")
<!--Main Container-->
<div class="container">
	<div class="ptb-60">
		<div class="row">
			<div>
				<div class="col-sm-12">
					<h1 class="title">Ensino / <i>Teaching</i></h1>
				</div>
			</div>
		</div>

		<div style="margin-top: 0px">
			<div id="quotes">
				<blockquote>
					<p>
						<strong>[2016 - Verão] Tópicos de Álgebra Linear</strong><br>
						Ementa do curso: <a href="https://uspdigital.usp.br/janus/componente/catalogoDisciplinasInicial.jsf?action=3&amp;sgldis=MAT4302">clique aqui</a>.<br>
						Listas de Exercícios: <a href="https://drive.google.com/folderview?id=0B9MVvzPCsNowZkVhVnFnNzdHMTA&amp;usp=drive_web">clique aqui</a>.						
					</p>
				</blockquote>
			</div>
		</div>
	</div>
</div>
@endsection