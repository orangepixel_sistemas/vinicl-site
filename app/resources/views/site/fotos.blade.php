<div class="section-portfolio">
	<h2 class="section-title">Fotos / <i>Pictures</i></h2>
	@if($extra)
		<div class="extra-content" style="margin-bottom: 15px">
			{!! $extra !!}
		</div>
	@endif
	<div class="projects-list">
		@foreach($imagens as $image)
			<div class="project-item">
				<a href="{{ $image->url }}" class="project-thumbnail nivobox" data-lightbox-gallery="portfolio" data-src="url('{{ $image->url }}')">
					<div class="project-description-wrapper">
						<div class="project-description">
							<h2 class="project-title">{{ $image->titulo }}</h2>
							<span class="see-more">{{ substr($image->data, 0, -3) }}</span>
						</div>
					</div>
				</a>
			</div>
		@endforeach
	</div>
</div>