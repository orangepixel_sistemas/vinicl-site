@extends("site.layouts.default")

@section("conteudo")
<div class="container home_container">
	<div class="ptb-60">

		<!--Intro -->
		<div class="row valign-wrapper">
			<div class="col-sm-6">
				<div class="home_icon_block">
					<!--Main Image-->
					<img class="img img-responsive" src="{!! asset('assets/images/home_author.jpg') !!}" alt="Web Developer">
					<!--Social icons-->
					<div class="home_social_icon">
						<a href="#">
							<span class="facebook_icon"></span>
						</a>
						<a href="#">
							<span class="twitter_icon"></span>
						</a>
						<a href="#">
							<span class="pin_icon"></span>
						</a>
						<a href="#">
							<span class="linkedin_icon"></span>
						</a>
					</div>
					<!--End Social icons-->
				</div>
			</div>
			<div class="col-sm-6 home_author_name">
				<div class="home_author_details">
					<!--Main Name-->
					<h1><span class="bolder">Maikel</span> Antônio Samuays</h1>
					<h3>Matemática - Geometria Diferencial<br /><i style="font-size: 20px !important">Mathematics - Differential Geometry</i></h3>
					<h4>
						Sobre Mim / About Me
					</h4>
				</div>
				<p class="home_para_hide">
					Possui graduação em Matemática pela Universidade Federal do Paraná (2008), mestrado em Matemática Aplicada pela Universidade Federal do Paraná (2011) e doutorado em Matemática pela Universidade de São Paulo (2015). Atualmente é Professor Adjunto A da Universidade Federal da Bahia. Tem experiência na área de Matemática, com ênfase em Geometria Diferencial.
				</p>
				<p><i>
					He graduated in mathematics from the Federal University of Paraná (2008), master's degree in Applied Mathematics from the Federal University of Paraná (2011) and doctorate in mathematics from the University of São Paulo (2015). He is currently Associate Professor of the Federal University of Bahia. He has experience in the area of Mathematics, with emphasis on differential geometry.
				</i></p>
				<div class="row valign-wrapper mb40">
					<div class="col-sm-12 mob_center">
						<a href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4481650D0" class="btn btn-primary system_btn quick_btn">Ver Meu Currículo Lattes</a>

						<a href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4481650D0" class="btn btn-primary system_btn quick_btn">See My Curriculum</a>								
					</div>
				</div>
			</div>
		</div>
		<!--End Intro-->

		
	</div>
</div>
@endsection