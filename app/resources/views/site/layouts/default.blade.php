<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Vinicius C. Laass - Matemático e Topologo Algébrico</title>
	<!-- Favicons
		================================================== -->
	<link rel="shortcut icon" href="{!! asset('assets/img/favicon.ico') !!}" type="image/x-icon">
	<link rel="icon" href="{!! asset('assets/img/favicon.ico') !!}" type="image/x-icon">
	<!-- / Favicons
		================================================== -->
	<!-- >> CSS
		============================================================================== -->
	<link href="{!! asset('assets/vendor/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">
	<!-- Google Web Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<!-- Font Awesome -->
	<link href="{!! asset('assets/vendor/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet">
	<!-- Nivo Lightbox -->
	<link href="{!! asset('assets/vendor/nivo-lightbox/nivo-lightbox.css') !!}" rel="stylesheet">
	<link rel="stylesheet" href="{!! asset('assets/vendor/nivo-lightbox/themes/default/default.css') !!}" type="text/css" />
	<!-- /Nivo Lightbox -->
	<!-- Perfect ScrollBar -->
	<link href="{!! asset('assets/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css') !!}" rel="stylesheet">
	<!-- owl carousel -->
	<link href="{!! asset('assets/vendor/owl.carousel/owl-carousel/owl.carousel.css') !!}" rel="stylesheet">
	<link href="{!! asset('assets/vendor/owl.carousel/owl-carousel/owl.theme.css') !!}" rel="stylesheet">
	<!-- Main Styles -->
	<link href="{!! asset('assets/css/styles.css') !!}" rel="stylesheet">
	<!-- >> /CSS
		============================================================================== -->
</head>
<body>
	{{ csrf_field() }}
	<!-- Page Loader
		========================================================= -->
	<div class="loader-container" id="page-loader">
		<div class="loading-wrapper">
			<div class="loader-animation" id="loader-animation">
				<span class="loader"><span class="loader-inner"></span></span>
			</div>
			<!-- Edit With Your Name -->
			<div class="loader-name" id="loader-name">
				Vinicius Casteluber Laass
			</div>
			<!-- /Edit With Your Name -->
			<!-- Edit With Your Job -->
			<p class="loader-job" id="loader-job">Matemático & Topólogo</p>
			<!-- /Edit With Your Job -->
		</div>
	</div>
	<!-- /End of Page loader
		========================================================= -->
	<!-- Main Content
		================================================== -->
	<section id="body" class="">
		<div class="container">
			<!-- MAIN MENU -->
			<div class="main-menu" id="main-menu">
				<ul class="main-menu-list">
					@foreach($posts as $post)
						@if($post->slug == "home")
							<li><a href="#" class="link-home">{!! $post->titulo !!}</i></a></li>
						@else
							<li><a href="#{{ $post->slug }}" class="link-page">{!! $post->titulo !!}</i></a></li>
						@endif
					@endforeach
				</ul>
			</div>
			<!-- /MAIN MENU -->

			@foreach($posts as $post)
				@include('site.post', ['post' => $post, 'imagens' => $imagens])
			@endforeach			
			
		</div>
	</section>
	<!-- /Main Content
		================================================== -->
	<!-- Contact Form - Ajax Messages
		========================================================= -->
	<!-- Form Sucess -->
	<div class="form-result modal-wrap" id="contactSuccess">
		<div class="modal-bg"></div>
		<div class="modal-content">
			<h4 class="modal-title"><i class="fa fa-check-circle"></i> Sucesso / <i>Success</i>!</h4>
			<p>Sua mensagem foi enviada! <i>Your message has been sent to us.</i></p>
		</div>
	</div>
	<!-- Contact Form - Ajax Messages
		========================================================= -->
	<!-- >> JS
		============================================================================== -->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="{!! asset('assets/vendor/jquery.min.js') !!}"></script>

	<script>
		var email_route = "{{ route('email') }}";
	</script>
	<style>
		.project-thumbnail {
			background-image: none !important;
		}
	</style>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="{!! asset('assets/vendor/bootstrap/js/bootstrap.min.js') !!}"></script>
	<script src="{!! asset('assets/vendor/validate.js') !!}"></script>
	<!-- Holder JS -->
	<script src="{!! asset('assets/vendor/holder.js') !!}"></script>
	<!-- Modal box-->
	<script src="{!! asset('assets/vendor/nivo-lightbox/nivo-lightbox.min.js') !!}"></script>
	<!-- /Modal Box -->
	<!-- Perfect ScrolBar -->
	<script src="{!! asset('assets/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') !!}"></script>
	<!-- /Perfect ScrolBar -->
	<!-- Owl Caroulsel -->
	<script src="{!! asset('assets/vendor/owl.carousel/owl-carousel/owl.carousel.js') !!}"></script>
	<!-- Cross-browser -->
	<script src="{!! asset('assets/vendor/cross-browser.js') !!}"></script>
	<!-- Main Scripts -->

	<script src="{!! asset('assets/js/main.js') !!}"></script>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="vendor/html5shiv.js"></script>
	<script src="vendor/respond.min.js"></script>
	<![endif]-->
	<!-- >> /JS
	============================================================================= -->
</body>
</html>
