<nav class="cd-side-navigation sidebar left">
	<ul>
		@foreach($items as $item)
		<li>
			<a href="{!! $item->url() !!}" @if(isset($item->attributes["class"]))
				class="selected"
			@endif
			>
				@if($item->icon)<i class="fa {{ $item->icon }}" style="font-size: 32px"></i>@endif
				<span>{!! $item->title !!}</i></span>
			</a>
		</li>
		@endforeach
	</ul>
</nav>