@extends("site.layouts.default")

@section("conteudo")
<div class="container">
	<div class="ptb-60">
		<div class="row">
			<div>
				<div class="col-sm-12">
					<h1 class="title">Pesquisa / <i>Research</i></h1>
					<p class="mb40">
						Assertively cultivate professional interfaces without synergistic etworks. Quickly erage existing customized ideas through client-based eliverables. Compellingly unleash fully tested outsourcing with prospective tunities. Uniquely maximize client-centric tals rather than focused web-readiness. Intrinsicly monetize reliable interfaces rather than multimedia based experiences. synergistic networks. Quickly rather than multimedia based Collaboratively synergize economically sound process improvements with functionalized e-commerce.
					</p>
				</div>
			</div>
		</div>
		<!--End Intro-->

		<div style="margin-top: 80px">
			<h3>Publicações / <i>Publications</i></h3>
			<div id="quotes">
				<!--1-->
				<blockquote>
					<p>
						D. L. Gonçalves, J. Guaschi, V. C. Laass, The Borsuk-Ulam property for homotopy class for selfmaps between surfaces of Euler characteristic zero (em redação / writing)
					</p>
				</blockquote>
				<!--2-->
				<blockquote>
					<p>
						V. C. Laass, A propriedade de Borsuk-Ulam para funções entre superfícies, Doctoral Thesis, IME-USP, 2015. (disponível <a href="http://www.teses.usp.br/teses/disponiveis/45/45131/tde-02102015-102952/pt-br.php">aqui</a> / <i>available <a href="http://www.teses.usp.br/teses/disponiveis/45/45131/tde-02102015-102952/en.php">here</a></i>)
					</p>
				</blockquote>
				<!--3-->
				<blockquote>
					<p>
						V. C. Laass, Grupos de Tranças do Espaço Projetivo, Master's Dissertation, ICMC-USP, 2011. (disponível <a href="http://www.teses.usp.br/teses/disponiveis/55/55135/tde-12052011-105031/pt-br.php">aqui</a> / <i>available <a href="http://www.teses.usp.br/teses/disponiveis/55/55135/tde-12052011-105031/en.php">here</a></i>)
					</p>
				</blockquote>
			</div>
		</div>

		<!--Main Content-->
		<div class="row">
			<div class="col-xs-12 col-sm-12">

				<!--Experience-->
				<div style="margin-top: 80px">
					<h3>Trabalho / <i>Work</i></h3>
					<div class="timeline timeline-left gray-blue">
						<!--1-->
						<div class="timeline-block">
							<i class="fa fa-suitcase"></i>
							<div class="timeline-icon"></div>
							<div class="timeline-content">
								<h4>Universidade Federal da Bahia, Instituto de Matemática / Departamento de Matemática</h4>
								<p style="min-height: 20px">Atualmente é Professor Adjunto A da Universidade Federal da Bahia. Tem experiência na área de Matemática, com ênfase em Geometria Diferencial.</p>
								<div class="timeline-date">2015 - atual
									<h4 class="system_font_color">Professor Adjunto A</h4>
								</div>
							</div>
						</div>
						<!--2-->
					</div>
				</div>
				<!--End of Experience-->

				<!--Education-->
				<div style="margin-top: 80px">
					<h3>Formação Acadêmica / <i>Formal Education</i></h3>
					<div class="timeline timeline-left gray-blue">
						<div class="timeline-block">
							<i class="fa fa-graduation-cap"></i>
							<div class="timeline-icon"></div>
							<div class="timeline-content">
								<h4>Doutorado em Matemática / <span class="italic">Ph.D in Mathematics</span></h4>
								<h5>Subvariedades Lagrangeanas Mínimas e Autossimilares no Espaço Paracomplexo</h5>
								<p>
								Orientador: Rosa Maria dos Santos Barreiro Chaves.<br/>
								Bolsista do(a): Fundação de Amparo à Pesquisa do Estado de São Paulo, FAPESP, Brasil.<br/>
								Palavras-chave: Subvariedades lagrangeanas; Espaço paracomplexo.<br/>
								Grande área: Ciências Exatas e da Terra<br/>
								</p>
								<div class="timeline-date">2011-2015
									<h4 class="system_font_color">Universidade de São Paulo, USP, Brasil</h4>
								</div>
							</div>
						</div>

						<div class="timeline-block">
							<div class="timeline-icon"></div>
							<div class="timeline-content">
								<h4>Mestrado em Matemática Aplicada / <span class="italic">Master's in Applied Mathematics</span></h4>
								<h5>O problema de Brezis - Nirenberg</h5>
								<p>
								Orientador: Jurandir Ceccon.<br />
								Bolsista do(a): Fundação Araucária, FA, Brasil. <br />
								Palavras-chave: Equações diferenciais parciais elípticas; método variacional; p-laplaciano; concentração de compacidade.<br />
								Grande área: Ciências Exatas e da Terra<br />
								</p>
								<div class="timeline-date">2009-2011
									<h4 class="system_font_color">Universidade Federal do Paraná, UFPR, Brasil</h4>
								</div>
							</div>
						</div>

						<div class="timeline-block">
							<div class="timeline-icon"></div>
							<div class="timeline-content">
								<h4>Graduação em Matemática / <span class="italic">Graduation in Mathematics</span></h4>
								<p style="height: 60px">&nbsp;</p>
								<div class="timeline-date">2005-2008
									<h4 class="system_font_color">Universidade Federal do Paraná, UFPR, Brasil</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End of Main Content-->
	</div>
</div>
<!--End Main Container-->
@endsection