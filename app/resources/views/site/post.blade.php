<div @if($post->slug == "home") id="section-home" @else id="{{ $post->slug }}" @endif class="section-vcardbody @if($post->slug == "home")section-home @else section-page @endif">
	@if($post->slug == "fotos")
		@include('site.fotos', ["imagens" => $imagens, "extra" => $post->conteudo])
	@else
		{!! $post->conteudo !!}
	@endif
</div>